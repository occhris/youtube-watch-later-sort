<?php

// Before doing this we must use the "Multiselect for YouTube" Firefox/Chrome addon to copy everything from "Watch Later" to a playlist called staging.
//  This is because the YouTube API does not let us work with Watch Later.
//  Maybe also pre-move some videos using this addon, as default YouTube quota only lets us process about 180 videos per day.

// Run on command line (at least initially)... "php go.php"
//  Follow oAuth prompt when requested
//  Can also give a parameter if we are supporting multiple oAuth keys

error_reporting(E_ALL);
ini_set('display_errors', '1');
set_time_limit(0);

chdir(__DIR__);

$id = 'default';
if (isset($_SERVER['argv'][1])) {
    $id = $_SERVER['argv'][1];
}
if (isset($_GET['id'])) {
    $id = $_GET['id'][1];
}

$ob = new YouTubeWatchLaterSort($id);
$ob->execute();

// Implemented using https://github.com/googleapis/google-api-php-client

// Docs...
//  https://developers.google.com/youtube/v3/guides/implementation/playlists
//  https://developers.google.com/youtube/v3/docs/playlists
//  https://developers.google.com/youtube/v3/docs/playlistItems

class YouTubeWatchLaterSort
{
    protected $service;
    protected $access_token;
    protected $id;

    public function __construct($id)
    {
        $this->id = $id;

        $token_filename = 'cached_token__' . $this->id . '.txt';

        if ((is_file($token_filename)) && (filemtime($token_filename) > time() - 3599)) {
            $cached_token = trim(file_get_contents($token_filename));
        } else {
            $cached_token = '';
        }

        if (!empty($cached_token)) {
            if (php_sapi_name() != 'cli') {
                header('Content-Type: text/plain');
            }

            $this->access_token = $cached_token;
            $access_token = $this->auth();
        } else {
            if (php_sapi_name() != 'cli') {
                throw new Exception('Must be run on the command line');
            }

            $access_token = $this->auth();
            file_put_contents($token_filename, $access_token);
            echo 'Cached token into ' . $token_filename . "\n";
        }
    }

    public function execute()
    {
        $playlists = $this->list_playlists();
        $staging_playlist = $this->find_staging($playlists);

        $videos = [];
        if (is_file('cached_item_ids.txt')) {
            $lines = file('cached_item_ids.txt');
            foreach ($lines as $line) {
                if (strpos($line, ':') !== false) {
                    list($video_id, $playlist_item_id, $channel_title) = explode(':', rtrim($line), 3);
                    $videos[] = [$video_id, $playlist_item_id, $channel_title];
                }
            }
        } else {
            $staging_playlist_items = $this->list_playlist($staging_playlist);
            foreach ($staging_playlist_items as $playlist_item) {
                $video_id = $playlist_item->snippet->resourceId->videoId;
                $playlist_item_id = $playlist_item->id;
                $channel_title = $playlist_item->snippet->videoOwnerChannelTitle;
                $videos[] = [$video_id, $playlist_item_id, $channel_title];
            }
            $this->save_cached_items_file($videos);
        }

        $videos_copy = $videos;
        foreach ($videos_copy as $i => $_) {
            list($video_id, $playlist_item_id, $channel_title) = $_;

            echo 'Doing ' . $video_id . "\n";
            flush();

            list($playlist, $is_new) = $this->find_or_create_playlist($playlists, $channel_title);

            if ($is_new) {
                sleep(5); // Takes a little time for things to sync

                $playlists[] = $playlist;
                $this->add_video_to_playlist($playlist, $video_id);
            } else {
                if (!$this->is_video_in_playlist($playlist, $video_id)) {
                    $this->add_video_to_playlist($playlist, $video_id);
                }
            }

            // Actually this wastes too many quota, can delete whole list manually at end $this->delete_video_from_playlist($playlist_item_id);

            echo 'Done ' . $video_id . "\n";
            flush();

            // Re-save cache file
            unset($videos[$i]);
            $this->save_cached_items_file($videos);
        }
    }

    protected function save_cached_items_file($videos)
    {
        $cache_file = fopen('cached_item_ids.txt', 'wb');
        foreach ($videos as $_) {
            list($video_id, $playlist_item_id, $channel_title) = $_;
            fwrite($cache_file, $video_id . ':' . $playlist_item_id . ':' . $channel_title . "\n");
        }
        fclose($cache_file);
    }

    protected function auth()
    {
        if (!file_exists(__DIR__ . '/vendor/autoload.php')) {
            throw new Exception(sprintf('Please run "composer require google/apiclient:~2.0" in "%s"', __DIR__));
        }
        require_once __DIR__ . '/vendor/autoload.php';

        $client = new Google_Client();
        $client->setApplicationName('YouTube Watch Later spreader');
        $client->setScopes([
            'https://www.googleapis.com/auth/youtube.readonly',
            'https://www.googleapis.com/auth/youtube.force-ssl',
        ]);

        $json_filename = 'client_secret__' . $this->id . '.json'; // https://developers.google.com/youtube/registering_an_application
        if (!is_file($json_filename)) {
            throw new Exception('Missing configuration file, ' . $json_filename . ' - set up oAuth key on Google API Console, and download JSON for it');
        }
        $client->setAuthConfig($json_filename);
        $client->setAccessType('offline');

        if (!empty($this->access_token)) {
            $access_token = $this->access_token;
            $client->setAccessToken($access_token);
        } else {
            // Request authorization from the user.
            $auth_url = $client->createAuthUrl();
            printf("Open this link in your browser:\n%s\n", $auth_url);
            print('Enter verification code: ');
            $auth_code = trim(fgets(STDIN));

            // Exchange authorization code for an access token.
            $_access_token = $client->fetchAccessTokenWithAuthCode($auth_code);
            $access_token = $_access_token['access_token'];
            var_dump($_access_token);
            $client->setAccessToken($access_token);
        }

        // Define service object for making API requests.
        $this->service = new Google_Service_YouTube($client);

        return $access_token;
    }

    protected function list_playlists()
    {
        $queryParams = [
            'mine' => true
        ];

        $playlists = [];

        do {
            $response = $this->service->playlists->listPlaylists('snippet', $queryParams);
            $playlists = array_merge($playlists, $response->items);

            $queryParams['pageToken'] = $response->nextPageToken;
        } while (!empty($response->nextPageToken));

        return $playlists;
    }

    protected function find_staging($playlists)
    {
        foreach ($playlists as $playlist) {
            if ($playlist->snippet->title == 'Staging') {
                return $playlist;
            }
        }

        throw new Exception('Could not find watch later playlist');
    }

    protected function list_playlist($playlist)
    {
        static $cache = array();
        if (isset($cache[$playlist->id])) {
            return $cache[$playlist->id];
        }

        $queryParams = [
            'playlistId' => $playlist->id,
        ];

        $playlist_items = [];

        do {
            $response = $this->service->playlistItems->listPlaylistItems('contentDetails,snippet', $queryParams);
            $playlist_items = array_merge($playlist_items, $response->items);

            $queryParams['pageToken'] = $response->nextPageToken;
        } while (!empty($response->nextPageToken));

        $cache[$playlist->id] = $playlist_items;

        return $playlist_items;
    }

    protected function find_or_create_playlist($playlists, $title)
    {
        foreach ($playlists as $playlist) {
            if ($playlist->snippet->title == $title) {
                return [$playlist, false];
            }
        }

        $playlist = new Google_Service_YouTube_Playlist();
        $playlist->setSnippet(new Google_Service_YouTube_PlaylistSnippet());
        $playlist->snippet->setTitle($title);
        $playlist->setStatus(new Google_Service_YouTube_PlaylistStatus());
        $playlist->status->setPrivacyStatus('private');

        $playlist = $this->service->playlists->insert('snippet,status', $playlist);
        return [$playlist, true];
    }

    protected function is_video_in_playlist($playlist, $video_id)
    {
        $playlist_items = $this->list_playlist($playlist);

        foreach ($playlist_items as $playlist_item) {
            if ($playlist_item->snippet->resourceId->videoId == $video_id) {
                return true;
            }
        }

        return false;
    }

    protected function add_video_to_playlist($playlist, $video_id)
    {
        $playlist_item = new Google_Service_YouTube_PlaylistItem();
        $playlist_item->setSnippet(new Google_Service_YouTube_PlaylistItemSnippet());
        $playlist_item->snippet->setPlaylistId($playlist->id);
        $playlist_item->snippet->setResourceId(new Google_Service_YouTube_ResourceId());
        $playlist_item->snippet->resourceId->setKind('youtube#video');
        $playlist_item->snippet->resourceId->setVideoId($video_id);

        $response = $this->service->playlistItems->insert('snippet', $playlist_item);
    }

    protected function delete_video_from_playlist($playlist_item_id)
    {
        $this->service->playlistItems->delete($playlist_item_id);
    }
}
